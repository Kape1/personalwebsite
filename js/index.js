"use strict";

let navButtonsArr = [];

// Controls which link is in active state.
function activeNav() {
    let listItems = $.id("navButtonList").getElementsByTagName("li");
    for (let i = 0; i < listItems.length; i++) {
        listItems[i].addEventListener("click", e => {
            for (let j = 0; j < listItems.length; j++) {
                listItems[j].className = "";
            }
            listItems[i].className = "active";
        });
    }
}

// Smooth scrolling.
function scrollViaTo(button, target) {
    $.id(button).addEventListener("click", e => {
        console.log(button + " pressed!");
        $.id(target).scrollIntoView({ block: 'start',  behavior: 'smooth' });
    });
}

function init() {
    activeNav();
    scrollViaTo("homeButton", "body");
    scrollViaTo("aboutButton", "aboutDiv");
    scrollViaTo("aboutButton2", "aboutDiv");
    scrollViaTo("projectsButton", "projectsDiv"); 
    scrollViaTo("workExperienceButton", "workExpDiv");
    scrollViaTo("contactButton", "contact");
    scrollViaTo("contactButton2", "contact");
}

window.onload = init;
