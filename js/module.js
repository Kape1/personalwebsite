"use strict";

let $ = (function () {
	return {
		id: function (id) {
			return document.getElementById(id);
		}
	};
})();
